<?php
require_once("config.inc.php");

if((isset($_SESSION['admin']) && ($_SESSION['admin']=="OK"))||(isset($_SESSION['homeworker']) && ($_SESSION['homeworker']=="OK"))){
	header("Location: home.php");
	exit();	
}
?>
<!DOCTYPE html>
<html>
<head>
<?php
include("template/metatag.php");
include("template/css.php");
include("template/js.php");
?>
</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">
        <div class="logo animate0 bounceIn"><img src="template/images/logo.png" alt="<?php echo $nome_applicazione; ?>" /></div>
        <form id="login" action="login.php" method="post">
            <div class="inputwrapper login-alert">
                <div class="alert alert-error">Invalid username or password</div>
            </div>
            <div class="inputwrapper animate1 bounceIn">
                <input type="text" name="username" id="username" placeholder="nome utente" />
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <input type="password" name="password" id="password" placeholder="password" />
            </div>
            
                <div class="inputwrapper animate2 bounceIn">
            <select name="database" id="database">
                <option value="ariota_exitprice">ExitPrice</option>
            </select>
        </div>
            
            <div class="inputwrapper animate3 bounceIn">
                <button name="submit">Accedi</button>
            </div>
            <div class="inputwrapper animate4 bounceIn">
                <label><input type="checkbox" class="remember" name="signin" /> Ricorda dati</label>
            </div>
            
        </form>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->

<div class="loginfooter">
    <p>&copy; <?php echo date("Y"); ?>. <?php echo $nome_applicazione; ?>. All Rights Reserved.</p>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>

</body>
</html>
