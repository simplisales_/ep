<?php
require_once('config.inc.php');
require_once('include/verifica_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include("template/metatag.php");
include("template/css.php");
include("template/js.php");
include("include/fancybox.php");
?>
</head>
<?php
	$query='select count(*) as totale_prodotti from prodotti join stocks on ID = prodotto_id where negozio_id='.$_SESSION['id_negozio'].';';
	$result = mysql_query($query) or die (mysql_error());
	$dettaglio = mysql_fetch_array($result,MYSQL_ASSOC);
	$totale_prodotti=$dettaglio['totale_prodotti'];
	$query='select sum(quantita_totale) as quantita_totale from stocks where negozio_id='.$_SESSION['id_negozio'].';';
	$result = mysql_query($query) or die (mysql_error());
	$dettaglio = mysql_fetch_array($result,MYSQL_ASSOC);
	$quantita_totale=$dettaglio['quantita_totale'];
	?>
<body>
<div class="page-container">
<?php include("template/sidebar.php"); ?>
           <!-- PAGE CONTENT -->
            <div class="page-content">
<?php include("template/top.php"); ?>

<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
<div class="page-title"><h2>Home</h2></div>
 <div class="row">
 	<div class="col-lg-4">
    	<div class="panel panel-info">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-credit-card"></i> Vendita al banco</h3></div>
            <div class="panel-body panel-180">
            	<p>Inserisci il codice del prodotto o l'EAN o utilizza il lettore barcode in dotazione per cercare il prodotto</p>
               
                <div class="row">
                	<div class="col-lg-9">
                   		<div class="form-group input-group">
                        	<span class="input-group-addon"><i class="fa fa-barcode fa-2x"></i></span>
                            <input type="text" required="" id="ean" name="ean" placeholder="Codice Prodotto o EAN" class="form-control">
	                    </div>
                    </div>
                    <div class="col-lg-2">
                    	<button class="btn btn-info" id="VenditaBancoSubmit" type="submit">Vai</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
	<div class="col-lg-4">
    	<div class="panel panel-danger">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Ordini</h3></div>
            <div class="panel-body panel-180">
            	<p>Visualizza gli ordini da gestire, genera le fatture e stampa i dettagli per la spedizione</p>
                <div class="row">
                	<div class="col-lg-6">
                    	<a class="btn btn-danger btn-block btn-lg" href="/ordini/ordini_elenco.php?stato=PENDING">Ordini da evadere</a>
                    </div>
                    <div class="col-lg-6">
                    	<a class="btn btn-danger btn-block btn-lg" href="/ordini/ordini_elenco.php">Storico ordini</a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="col-lg-4">
    	 <div class="panel panel-primary">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-briefcase"></i> Prodotti in vendita</h3></div>
            <div class="panel-body panel-180">
            	<span class="pull-left"><i class="fa fa-inbox fa-5x"></i></span>
				<span class="pull-right"><h4><i class="fa fa-caret-up"></i> Numero prodotti: <?php echo $totale_prodotti;?></h4></span><br>
				<span class="pull-right"><h4><i class="fa fa-caret-up"></i> Quantità totale: <?php echo $quantita_totale;?></h4></span>
           <div class="clearfix"></div>
           
           <a class="btn btn-block btn-primary btn-lg" href="/prodotti/prodotti_elenco.php">
           		<span class="pull-left">Esplora elenco prodotti</span>
                <span class="pull-right"><i class="fa fa-arrow-right"></i></span>
                <div class="clearfix"></div>
           </a>
                    
            </div>
        </div>    
    </div>       
 </div>
 
  <div class="row">
 	<div class="col-lg-4">
    	<div class="panel panel-warning">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i> Inserisci prodotto</h3></div>
            <div class="panel-body panel-180">
            	<p>Inserisci un nuovo prodotto. Attenzione, il nuovo prodotto sarà disponibile entro pochi minuti dall'inserimento</p>
                <div class="row">
                	<div class="col-lg-12">    	                
      	        		<a class="btn btn-warning btn-block btn-lg" href="/prodotti/prodotto_inserisci.php">
                           	<span class="pull-left">Inserisci prodotto</span>
	    	                <span class="pull-right"><i class="fa fa-arrow-right"></i></span>    
    	                    <div class="clearfix"></div>
                        </a>
	                </div>
                </div>
            </div>
        </div>
    </div>
	<div class="col-lg-4">
    	<div class="panel panel-success">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-truck"></i> Forniture</h3></div>
            <div class="panel-body panel-180">
            	<p>Inserisci una nuova fornitura con prodotti già presenti sullo store. Per prodotti non presenti sullo store utilizzare la sezione "Inserisci Nuovo Prodotto"</p>
				<div class="row">
                	<div class="col-lg-6">
                    	<a class="btn btn-success btn-block btn-lg" href="/prodotti/prodotto_inserisci.php">Nuova fornitura</a>
                    </div>
                    <div class="col-lg-6">
                    	<a class="btn btn-success btn-block btn-lg" href="/forniture/forniture_elenco.php">Elenco forniture</a>
                    </div>
                </div>                
            </div>
        </div>
    </div>    
    <div class="col-lg-4">
    	 <div class="panel panel-default">
        	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Statistiche</h3></div>
            <div class="panel-body panel-180">
            <div class="row">
                            <div class="col-md-4">
                            <span style="font-size:18px;">Totale</span>
                            <h2>0</h2></div>
                            <div class="col-md-4">
                            <span style="font-size:18px;">Oggi</span>
                            <h2>0</h2></div>
                            <div class="col-md-4">
                                <span style="font-size:18px;">Andamento</span>
                            <h2><i class="glyph-icon icon-arrow-up font-green"></i>
                            0%</h2></div>
                        </div>
                        
                        <a class="btn btn-block btn-info btn-lg">
                        	<span class="pull-left">Dettagli</span>
                            <span class="pull-right"><i class="fa fa-arrow-right"></i></span>
                            <div class="clearfix"></div>
                        </a>
            </div>
            
                       
        </div>    
    </div>       
 </div>
 
                </div>

</div>
</div>
<script>
$('#VenditaBancoSubmit').click(function(){
	var ean = $('#ean').val();
	
	$.fancybox.open([{
        href : '/prodotti/prodotti_modifica_quantita.php?valore=' + ean,
	    title : 'Modifica quantità',
		type: 'iframe',
		autoSize : false,
    width    : "50%",
	height   : "80%"
    }]);
})

</script>
</body>
</html>
