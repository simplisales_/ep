<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo $url_applicazione; ?>/"><?php echo $nome_applicazione; ?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
<?php /*?>                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">John Doe</div>
                                <div class="profile-data-title">Web Developer/Designer</div>
                            </div>
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li><?php */?>
                    <li class="xn-title">Menu</li>
                    <li class="active">
                        <a href="<?php echo $url_applicazione; ?>/home.php"><span class="fa fa-desktop"></span> <span class="xn-text">Home</span></a>                        
                    </li>       <?php if($_SESSION['negozio_livello']=='ADMIN'){?>             
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Negozi</span></a>
                        <ul>
                            <li><a href="/negozi/negozio_inserisci.php"><span class="fa fa-plus"></span> Nuovo negozio</a></li>
                            <li><a href="/negozi/negozi_elenco.php"><span class="fa fa-list"></span> Elenco negozi</a></li>
                        </ul>
                    </li>
					<? } ?>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-barcode"></span> <span class="xn-text">Prodotti</span></a>
                        <ul>
						<?php if($_SESSION['negozio_livello']=='ADMIN'){?>
						<li><a href="/prodotti/prodotti_elenco.php"><i class="fa fa-list"></i> Elenco prodotti</a></li><?php }?>
                            <li><a href="/prodotti/prodotti_elenco.php?id_negozio=<?php echo $_SESSION['id_negozio'];?>"><i class="fa fa-star"></i> I miei prodotti</a></li>
                            <li><a href="/prodotti/prodotto_inserisci.php"><i class="fa fa-plus"></i> Aggiungi prodotto</a></li>
                        </ul>                        
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-truck"></span> <span class="xn-text">Forniture</span></a>                        
                        <ul>
                            
<?php if($_SESSION['negozio_livello']=='ADMIN'){?>                            <li><a href="/forniture/forniture_elenco.php"><span class="fa fa-list"></span> Elenco forniture</a></li>
<?php }?>
<li><a href="/forniture/forniture_elenco.php?id_negozio=<?php echo $_SESSION['id_negozio'];?>"><span class="fa fa-star"></span> Le mie Forniture</a></li>
<li><a href="/prodotti/prodotto_inserisci.php"><span class="fa fa-plus"></span> Nuova fornitura</a></li>
                        </ul>
                    </li>                    
                    <li class="xn-openable">
                        <a href="tables.html"><span class="fa fa-shopping-cart"></span> <span class="xn-text">Ordini</span></a>
                        <ul>                            
                            <?php if($_SESSION['negozio_livello']=='ADMIN'){?><li><a href="/ordini/ordini_elenco.php"><span class="fa fa-list"></span> Elenco ordini</a></li>
							<?php } ?>
							<li><a href="/ordini/ordini_elenco.php?id_negozio=<?php echo $_SESSION['id_negozio'];?>"><span class="fa fa-star"></span> I miei ordini</a></li>
							<li><a href="/ordini/ordini_elenco.php?stato=PENDING&id_negozio=<?php echo $_SESSION['id_negozio'];?>"><span class="fa fa-bell-o"></span> Ordini da evadere</a></li>
							<!--
                            <li><a href="table-datatables.html"><span class="fa fa-sort-alpha-desc"></span> Data Tables</a></li>
                            <li><a href="table-export.html"><span class="fa fa-download"></span> Export Tables</a></li>    -->                         
                        </ul>
                    </li>
					<?php if($_SESSION['negozio_livello']=='ADMIN'){?>  
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Statistiche</span></a>
                       <!-- <ul>
                            <li><a href="charts-morris.html"><span class="xn-text">Morris</span></a></li>
                            <li><a href="charts-nvd3.html"><span class="xn-text">NVD3</span></a></li>
                            <li><a href="charts-rickshaw.html"><span class="xn-text">Rickshaw</span></a></li>
                            <li><a href="charts-other.html"><span class="xn-text">Other</span></a></li>
                        </ul>-->
                    </li> 
					<?php } ?>
					<!--                
                    <li>
                        <a href="maps.html"><span class="fa fa-envelope"></span> <span class="xn-text">Messaggi</span></a>
                    </li>            -->        
                    
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->