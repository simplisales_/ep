<!-- CSS INCLUDE -->        
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,500,600,700&subset=latin,latin-ext" type='text/css'>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/jquery/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/bootstrap/bootstrap.min.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/fontawesome/font-awesome.min.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/summernote/summernote.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/codemirror/codemirror.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/nvd3/nv.d3.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/mcustomscrollbar/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/blueimp/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/rickshaw/rickshaw.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/animate/animate.min.css">
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $Css; ?>/theme-dark.css"/>

<!-- EOF CSS INCLUDE -->   