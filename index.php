<?php
require_once("config.inc.php");

if((isset($_SESSION['admin']) && ($_SESSION['admin']=="OK"))||(isset($_SESSION['homeworker']) && ($_SESSION['homeworker']=="OK"))){
	header("Location: home.php");
	exit();	
}
?>
<!DOCTYPE html>
<html>
<head>
<?php
include("template/metatag.php");
include("template/css.php");
include("template/js.php");
?>
</head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated zoomIn">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Accedi</strong> al tuo account</div>
                    <form action="login.php" class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="username" class="form-control" placeholder="nome utente"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password" placeholder="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Password dimenticata?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Accedi</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2015 WHManager
                    </div>
                    <div class="pull-right">
                        <a href="#">Chi siamo</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contattaci</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>






