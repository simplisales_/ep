<?php
require_once("config.inc.php"); 
require_once("include/verifica_login.php");

/*$result = $AEM_DB->get_results("select COALESCE(sum(quantita_totale),0) as quantita from stocks where negozio_id = '".$_SESSION['negozio_id']."';");

      $row = $result[0];
        $sum = $row['quantita'];
	$_SESSION['num_prodotti']= $sum ;

	$_SESSION['venduti']= 0;
       
                $query="select COALESCE(sum(vendita_quantita),0) as quantita from vendite where negozio_id = '".$_SESSION['negozio_id']."' and vendita_data = '".date("Y-m-d")."';";

	$_SESSION['venduti_oggi']= 0;
        
        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('yesterday'));

 
	$_SESSION['andamento']= 0;*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include("template/metatag.php");

include("template/css.php");
include("template/js.php");
?>
</head>
<body>
<div class="mainwrapper">

<?php  
include('template/top.php');
include('template/sidebar.php');
?>
<div class="rightpanel">
 <ul class="breadcrumbs">
            <li><a href="dashboard.html"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Home</li>

        </ul>
 <?php include ('template/page_title.php');?>            
 <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                <div class="span4">
<div class="widgetbox">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle"><i class="iconfa-credit-card"></i> Vendita al banco</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                <div class="span4">
<div class="widgetbox box-danger">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle"><i class="iconfa-shopping-cart"></i> Ordini</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                <div class="span4">
<div class="widgetbox box-inverse">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle"><i class="inconfa-briefcase"></i> Prodotti in vendita</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                <div class="row-fluid">
                
                <div class="span4">
<div class="widgetbox box-warning">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle">Inserisci prodotto</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                <div class="span4">
<div class="widgetbox box-success">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle">Forniture</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                <div class="span4">
<div class="widgetbox box-info">                        
                        <div class="headtitle">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <h4 class="widgettitle">Statistiche</h4>
                        </div>
                        <div class="widgetcontent">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div><!--widgetcontent-->
                        </div>
                </div>
                
                </div>
                
                
                
                </div>
             </div>
  </div>
</div>                

</div>
    </body>
</html>

