<?php
require_once("../config.inc.php");
require_once("../include/verifica_login.php");

$id_fornitura=$_GET['id_fornitura'];
$id_prodotto=$_GET['id_prodotto'];

$fornitura=new Fornitura($id_fornitura); //aggiungo la fornitura
$fornitura->elimina_prodotto($id_prodotto);

header("Location: /prodotti/prodotto_inserisci.php");
