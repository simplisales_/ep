<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');


$query_max_forniture = "SELECT fornitura_id FROM forniture where negozio_id = ".$_SESSION['id_negozio']." AND fornitura_stato=0 order by fornitura_id DESC limit 1;";

$result=mysql_query($query_max_forniture);

if(mysql_num_rows($result)>0){
	$res_max_forniture = mysql_fetch_row($result);
	$id_fornitura=$res_max_forniture[0];
	$fornitura = new Fornitura($id_fornitura);
}else{
	$valori = array();
	$valori['negozio_id']=$_SESSION['id_negozio'];
	$valori['data_inserimento']=$data_odierna;
	$valori['data_esito']='';
	$valori['fornitura_stato']=0;
	$fornitura = new Fornitura($valori);
	$fornitura->inserisci();
	$id_fornitura=$fornitura->fornitura_id;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include("../template/metatag.php");
include("../template/css.php");
include("../template/js.php");
include("../include/bootstrap-duallistbox.php");
?>
</head>
<body>
<div class="page-container">
<?php include("../template/sidebar.php"); ?>
<div class="page-content">
<?php 
include("../template/top.php"); 
include("../template/breadcrumb.php");
?>
<div class="page-content-wrap">
	<div class="col-lg-12">
    <h2>Inserisci prodotto  <div class="btn-group pull-right">
<?php 
$elenco_prodotti = $fornitura->get_elenco_prodotti(); 

$numero_prodotti = mysql_num_rows($elenco_prodotti);
?>
            <button id="Forniture" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="false" href="#"><i class="fa fa-truck fa-2x"></i> <span class="badge"><?php echo $numero_prodotti; ?></span></button>
                                                    <div class="dropdown-menu">
                                                   	  <div class="panel">
                                                        	<div class="panel-heading panel-danger"><h3 class="panel-title"><i class="fa fa-truck"></i> Prodotti </h3></div>
                                                           <div class="panel-body"><div style="max-height:200px;width:400px;overflow-y:auto;">
                                                           <table class="table table-striped table-bordered" id="TabellaForniture">
                                                           		<thead>
                                                                	<tr>
                                                                    	<th class="header">Foto</th>
                                                                        <th class="header">Nome</th>                                                                        <th class="header">Qtà</th>
                                                                        <th class="header">Azioni</th>	
                                                                    </tr>
                                                                </thead>
	                                                           <tbody>
                                                           
<?php



while($array_prodotti = mysql_fetch_array($elenco_prodotti)){
	if($array_prodotti['prod_parent_id']=='') $idmodifica=$array_prodotti['ID'];
	else $idmodifica=$array_prodotti['prod_parent_id'];
	if(!empty($array_prodotti['prod_immagine'])){
		$immagine_prodotto = 'img/'.$array_prodotti['prod_immagine'];
	}else{
		$immagine_prodotto = $url_immagini_applicazione."/immagine_non_disponibile.jpg";
	}
	
	?>
    <tr>
    	<td align="center"><img  width="40" height="auto" src="<?php echo $immagine_prodotto; ?>" /></td>
        <td><?php echo $array_prodotti['prod_nome']; ?><?php if($array_prodotti['pa_taglia']!='') echo '<span class="label label-default label-form">'.$array_prodotti['pa_taglia'].'</span>';if($array_prodotti['pa_colore']!='') echo '&nbsp;<span class="label label-primary label-form">'.$array_prodotti['pa_colore'];?></span></td>
        <td><?php echo $array_prodotti['quantita']; ?></td>
        <td align="center"><a class="fa fa-edit btn btn-success btn-sm" href="prodotto_modifica.php?id_prodotto=<?php echo $idmodifica; ?>&id_fornitura=<?php echo $array_prodotti['fornitura_id']; ?>"></a> <a class="fa fa-trash-o btn btn-danger btn-sm" href="prodotto_rimuovi.php?id_prodotto=<?php echo $array_prodotti['ID']; ?>&id_fornitura=<?php echo $array_prodotti['fornitura_id']; ?>"></a></td>
	</tr>
    <?php
}

?>                  </tbody></table>                                    	
                                                       		</div></div>
                                                        </div> 
                                                    </div>
                                                </div>
    
        </h2>
    <div class="alert alert-info">
    	<a class="btn btn-primary" href="/cronjobs/sincronizza_dati.php?passwd=exit2015&redirect=1"><i class="fa fa-refresh"></i> Sincronizza dati</a>
    </div>
<form method="post" action="prodotto_inserisci_exec.php" enctype="multipart/form-data">    
	<?php include("inc/prodotto_form.php"); ?>
</form>
    </div>
	<div class="col-lg-12"><?php echo MostraBackLink(); ?></div>
</div>

</div>
</div>
<p>
  <script>function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#prev_prod_immagine').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#prod_immagine").change(function(){
        readURL(this);
    });
	
	</script>
</p>
<p>&nbsp; </p>
<script>
$(document).ready(function() {
	
	
$(document).on('click','a.clona',function(){
	elemento=$('.variante:last').clone();
	$('.variante:last').after(elemento);
 });
 
$(document).on('click','a.aggiungi_variante',function(){
	elemento=$('.variante:last').clone().find('input').val('').end();
	$('.variante:last').after(elemento);
 });
 
 $(document).on('click','.pa_colore',function(){
	$( ".pa_colore" ).autocomplete({
		source: "../dati/get_colori.php",
	    select: function(event, ui) { 
			var colore=ui.item.value;
			$(this).val(colore);
		}		
	});
 });
 
  $(document).on('click','.pa_taglia',function(){
	$('.pa_taglia').autocomplete({
		source: "../dati/get_taglia.php",
	    select: function(event, ui) { 
			var taglia=ui.item.value;
			$(this).val(taglia);
		}		
	});
 });
 
	$("#ids_categorie").bootstrapDualListbox();			
	/*	
	$( ".pa_colore" ).autocomplete({
		source: "../dati/get_colori.php",
	    select: function(event, ui) { 
			var colore=ui.item.value;
			$(this).val(colore);
		}		
	});
	
	$('.pa_taglia').autocomplete({
		source: "../dati/get_taglia.php",
	    select: function(event, ui) { 
			var taglia=ui.item.value;
			$(this).val(taglia);
		}		
	});*/

	$('#prod_id_marca').autocomplete({
		source: "../dati/get_brand.php",
	    select: function(event, ui) { 
			var brand=ui.item.value;
			$(this).val(brand);
		}		
	});	
	
	$('#SKU').autocomplete({
		source: "../dati/get_valori.php?field=SKU&escludi_varianti=1",
	    select: function(event, ui) { 
			var SKU=ui.item.value;
			var autocompleta=1;			
			$(this).val(SKU);
			
			AutoCompleta('SKU',SKU);			
		},
		change:	function(event, ui) { 
					if(autocompleta!=1){
					cerca($(this).attr('id'),$(this).val());
					}
				}			
	});	
	
	$('#prod_codice').autocomplete({
		source: "../dati/get_valori.php?field=prod_codice&escludi_varianti=1",
	    select: function(event, ui) { 
			var prod_codice=ui.item.value;
			var autocompleta=1;			
			$(this).val(prod_codice);

			AutoCompleta('prod_codice',prod_codice);			
		},
		change:	function(event, ui) { 
					if(autocompleta!=1){
					cerca($(this).attr('id'),$(this).val());
					}
				}			
	});		
	
	
	$('#prod_EAN_13').autocomplete({
		source: "../dati/get_valori.php?field=prod_EAN_13&escludi_varianti=1",
	    select: function(event, ui) { 
			var prod_EAN_13=ui.item.value;
			var autocompleta=1;			
			$(this).val(prod_EAN_13);
			
			AutoCompleta('prod_EAN_13',prod_EAN_13);
		},
		change:	function(event, ui) { 
					if(autocompleta!=1){
					cerca($(this).attr('id'),$(this).val());
					}
				}			
	});			
		var autocompleta=0;
	$('#prod_nome').autocomplete({
		source: "../dati/get_modello.php?escludi_varianti=1",
	    select: function(event, ui) { 
			var modello=ui.item.value;
			var autocompleta=1;
			$(this).val(modello);
			
			AutoCompleta('prod_nome',modello);
		},
		change:	function(event, ui) { 
					if(autocompleta!=1){
					cerca($(this).attr('id'),$(this).val());
					}
				}	
	});	
	
	function cerca(nome_campo, valore_campo){
		if($('#prod_parent_id').val()!=''){
					var temp=valore_campo;
					$('#prod_tipo').val('');
					$('#prod_nome').val('');
					$('#SKU').val('');
					$('#prod_codice').val('');
					$('#prod_EAN_13').val('');
					$('#prod_id_marca').val('');
					CKEDITOR.instances['prod_descrizione'].setData('');
					CKEDITOR.instances['prod_descrizione'].setReadOnly(false);	
					$('#prev_prod_immagine').attr('src','');
					$('#prod_parent_id').val('');
					$('#boxvarianti').show();
					$('.pa_colore').show();
					$('.pa_taglia').show();
					$('#'+nome_campo).val(temp);
		}					
					AutoCompleta(nome_campo,valore_campo);
	}
	

	
	function AutoCompleta(nome_campo,valore_campo){

		var obj='';
		
		var richiesta = $.ajax({
                            url: "../dati/get_prodotto.php", 
                            type: "POST",
                            data: {
								nome_campo : nome_campo,
								valore_campo : valore_campo
							},
                            dataType: "text"
                          });
 
	       richiesta.done(function(data) { 
            	obj = jQuery.parseJSON(data);

				$('#prod_nome').val(obj.prod_nome);
				$('#prod_tipo').val(obj.prod_tipo);
				$('#SKU').val(obj.SKU);
				$('#prod_codice').val(obj.prod_codice);
				$('#prod_EAN_13').val(obj.prod_EAN_13);
				$('#prod_id_marca').val(obj.prod_marca);
				CKEDITOR.instances['prod_descrizione'].setData(obj.prod_descrizione);
				CKEDITOR.instances['prod_descrizione'].setReadOnly(true);			
				$('#prev_prod_immagine').attr('src','img/'+obj.prod_immagine);
				$('#prod_parent_id').val(obj.prod_parent_id);
				if((obj.colore==0)&&(obj.taglia==0)) $('#boxvarianti').hide();
				else $('#boxvarianti').show();
				if (obj.colore==0) $('.pa_colore').hide();
				else $('.pa_colore').show();
				if (obj.taglia==0) $('.pa_taglia').hide();
				else $('.pa_taglia').show();
        	});

        	richiesta.fail(function(jqXHR, textStatus) {
            	alert( "Request failed: " + textStatus );
	        });	
	}


     $('#prod_tipo').change(function(){
 var prod_type=$('#prod_tipo').val();
 
    if(prod_type=='simple'){
  $('#boxvarianti').hide();    
  $('#qta').show();
 }else if(prod_type=='variable'){
  $('#boxvarianti').show();
  $('#qta').hide();
 }
})




$('#Forniture').dropdownHover();
	
})

$(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});

function InviaFornitura(){
	var modello = $('#prod_nome').val();
	if(modello==''){
		$('#FormInviaFornitura').submit();
	}else{
		window.alert('Il prodotto corrente non è stato salvato');	
		return false;
	}
}


  </script>
</body>
</html>
