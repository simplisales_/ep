<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');

if(empty($_GET['id'])){
	die("Prodotto non definito");	
}else{
	$id_prodotto = $_GET['id'];
	
	$prodotto = new Prodotto($id_prodotto);	

}
?>
<head>
<?php
include("../template/metatag.php");
include("../template/css.php");
include("../template/js.php");
?>
</head>
<body>
<div class="page-container">
<div class="page-content">
<?php 
?>

	<div class="col-lg-12">
	<h2><?php echo $prodotto->prod_nome;?></h2></div>

    <div class="col-gl-12">
    	
		<?php $negozio = new Negozio($_SESSION['id_negozio']); ?>       
            
</div>   
<div class="col-lg-12">

		<div class="panel panel-primary">
	   		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-search"></i> Gestione scorte prodotto</h3></div>
		    <div class="panel-body">
            <div style="width:50%;float:left">
                 	<div class="col-lg-12"><img class="img-responsive" width="200px;" src="img/<?php echo $prodotto->prod_immagine?>"/></div>
			</div>                    
<div class="row" style="width:50%;float:left">   
<?php
$stock=new Stock($prodotto->ID, $_SESSION['id_negozio']);
?>

<h4>Quantità corrente: <?php echo $stock->quantita_totale; ?></h4>
<div class="row">
		<form method="post" id="form_inserimento" action="prodotto_modifica_quantita-exec.php">   
        <div class="form-group" style="width:50%">
			<label>Qtà venduta:</label>           
			<input type="text" id="qta" value="" name="qta" class="form-control" />
            <input type="hidden" name="id_stock" value="<?php echo $stock->stock_id; ?>">
		</div>
			<button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-save"></i> Salva</button>                    
	    </form>
		</div></div>

</div>


 
</div>
</div>
</div></div>

</body>


