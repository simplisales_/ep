	<div class="panel panel-primary">
	   		<div class="panel-heading">            
            	<div class="col-lg-6 pull-left">
	    	        <h3 class="panel-title"><i class="fa fa-search"></i> Anagrafica prodotto</h3>
				</div>
   			                    
            </div>
		    <div class="panel-body">
			  <div class="form-horizontal" id="form_inserimento" action="">
                  <div class="row">
                  	<label class="control-label col-md-1">Tipo</label>
                    <div class="form-group col-md-3">
                    	<select class="form-control" name="prod_tipo" id="prod_tipo" required>
                        	<option value=""  selected="selected" disabled="disabled">Seleziona &raquo;</option>
                        	<option value="simple">Semplice</option>
                            <option value="variable">Variante</option>
                        </select>
                    </div>


					<label class="control-label col-md-1">Modello:</label>
					<div class="form-group col-md-3">
					    <input type="text" id="prod_nome" name="prod_nome" required="required" placeholder="" class="form-control" />
					</div>                  
					<label class="control-label col-md-1" for="">Brand:</label>
					<div class="form-group col-md-3">	
	                    <input class="form-control prod_id_marca" required="required" id="prod_id_marca" name="prod_id_marca" />		
					</div>				      			                  
				  </div>
				  <div class="row">
				      <label class="control-label col-md-1" for="">SKU:</label>
					  <div class="form-group col-md-3">
					    <input type="text" id="SKU" name="SKU" readonly placeholder="" class="form-control" />
	    		    </div>
				      <label class="control-label col-md-1" for="">Codice:</label>
			 		  <div class="form-group col-md-3">
					    <input type="text" id="prod_codice" name="prod_codice" placeholder="" class="form-control" />
	     			</div>
				     					<label class="control-label col-md-1" for="prod_EAN_13">EAN 13:</label>
					<div class="form-group col-md-3"><input type="text" id="prod_EAN_13" name="prod_EAN_13" class="form-control" /></div>	

		 		 </div>
				 <hr>
				 <div class="row">                     
	                                                  
	           
					<div class="form-group col-md-2">
				    Prezzo (IVA Escl.)
					</div>
					<div class="form-group col-md-2">
				    Prezzo Listino:
					</div>
                    <div class="form-input col-md-2">Qtà:</div>   
                    <?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>       
						<div class="form-input col-md-2">Prezzo di Vendita</div>
					<?php } ?>                    
                </div>
                 <div class="row">                     
					<div class="form-group col-md-2">
				    <input type="number" step="0.01" value="" id="prod_prezzo_forn" name="prod_prezzo_forn" class="form-control" />
					</div>
					<div class="form-group col-md-2">
				    <input type="number" step="0.01" value="" id="prod_prezzo_list" name="prod_prezzo_list" class="form-control" />
					</div>
                    <div class="form-input col-md-2"><input type="number" step="1" id="qta" name="qta" class="form-control" /></div>   
                    <?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>       
						<div class="form-input col-md-2"><input name="prod_prezzo"  class="form-control" /></div>
					<?php } ?>                    
                </div>
				</div></div></div>  
				<div class="panel panel-warning" id="boxvarianti" style="display:none;">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-table"></i> Varianti</h3></div>
	    <div class="panel-body">
           <div class="row">
        		<div class="form-group col-md-1 pa_colore"><label class="control-label pa_colore" for="pa_colore">Colore</label></div>
     			<div class="form-group col-md-1 pa_taglia"><label class="control-label pa_taglia" for="pa_taglia">Taglia</label></div>
                <div class="form-group col-md-2"><label class="label-description" for="var_SKU">SKU</label></div>
			    <div class="form-group col-md-2"><label class="label-description" for="">Codice</label></div>
			    <div class="form-group col-md-2"><label class="label-description" for="">EAN13</label></div>
			    <div class="form-group col-md-1"><label class="label-description" for="">Prezzo</label></div>
			    <div class="form-group col-md-1"><label class="label-description" for="">Qtà</label></div>                
           </div>
		   <div class="row variante">			
			  <div class="form-group col-md-1 pa_colore"><input class="form-control pa_colore" id="pa_colore" name="pa_colore[]" /></div>
			  <div class="form-group col-md-1 pa_taglia"><input class="form-control pa_taglia" id="pa_taglia" name="pa_taglia[]" /></div>
			  <div class="form-group col-md-2"><input class="form-control" id="var_SKU" name="var_SKU[]" /></div>
			  <div class="form-group col-md-2"><input class="form-control" id="var_codice" name="var_codice[]" /></div>
			  <div class="form-group col-md-2"><input class="form-control" id="var_EAN13" name="var_EAN13[]" /></div>  
			  <div class="form-group col-md-1"><input class="form-control" id="var_prezzo" name="var_prezzo[]"></div>
			  <div class="form-group col-md-1"><input class="form-control" id="var_quant" name="var_quant[]"></div>
			  <div class="form-input col-md-2">
			      <a class="btn btn-sm btn-primary aggiungi_variante" title=""><i class="fa fa-plus"></i></a>
				   <a class="btn btn-sm btn-info clona" title=""><i class="fa fa-copy"></i></a>
				  <a onclick="$(this).parent().parent().remove();" class="btn btn-sm btn-danger" title=""><i class="fa fa-trash-o"></i></a>
			  </div>     			  
		  </div>
	    </div>
      </div>
				
				<?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>  
				<div class="panel panel-primary">
	   		<div class="panel-heading">            
            	<div class="col-lg-6 pull-left">
	    	        <h3 class="panel-title"><i class="fa fa-search"></i> Categorie</h3>
				</div>
   			                    
            </div>
		    <div class="panel-body">
                <div class="row"><div class="col-lg-12">
                	<?php 
					$query_categorie = mysql_query("SELECT * FROM categorie ORDER BY categoria_nome ASC") or die (mysql_error());
					?>
                    <select size="10" id="ids_categorie" multiple="multiple" name="prod_ids_categorie[]">
                    <?php	while($res_categorie=mysql_fetch_array($query_categorie)){	?>
                    	<option value="<?php echo $res_categorie['categoria_wp_id']; ?>"><?php echo $res_categorie['categoria_nome']; ?></option>
                    <?php	}	?>
                    </select>
                </div></div>
				</div></div>
				<?php } ?>  
		

	      
      <div class="panel panel-success">
      	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i> Descrizione</h3></div>
        <div class="panel-body">
      	                 <div class="row">

	                 <div class="form-input col-md-12">
	                 	<textarea class="ckeditor" name="prod_descrizione" id="prod_descrizione"></textarea>
                     </div>
                 </div>
        
        </div>
      </div>  
        
        
      
      <div class="panel panel-danger">
      	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-camera"></i> Immagini</h3></div>
	    <div class="panel-body">
			  <div class="row">
				  <div class="col-md-6"><input type="file" name="prod_immagine" id="prod_immagine"></div>
				  <div class="col-md-6"><img alt="your image" class="img-thumbnail" src="#" id="prev_prod_immagine"></div>
		  	  </div>   
	    </div>
      </div>
      <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Salva e continua</button>
	  <a onclick="InviaFornitura()" class="btn btn-danger"><i class="fa fa-truck"></i> Invia fornitura</a>
<input type="hidden" name="prod_parent_id" id="prod_parent_id" value="" />
<input type="hidden" name="fornitura_id" value="<?php echo $id_fornitura; ?>" />
</form>      
</div>
<form id="FormInviaFornitura" method="post" action="/forniture/fornitura_cambia_stato.php">
<input type="hidden" name="id_fornitura" value="<?php echo $id_fornitura; ?>" />
<input type="hidden" name="stato" value="1" />