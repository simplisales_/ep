<?php
require_once("../config.inc.php");
require_once("../include/verifica_login.php");

$colori=$_POST['pa_colore'];
$taglie = $_POST['pa_taglia'];
$var_quant=$_POST['var_quant'];
$var_prezzo=$_POST['var_prezzo'];

$c=0;
$c=max(sizeof($_POST['pa_colore']),sizeof($_POST['pa_taglia']));
$indici=array();
for ($i=0; $i<$c; $i++)
{
	if(($colori[$i]!="")||($taglie[$i]!="")){
		array_push($indici, $i);
	}
}

// 1-IDENTIFICO IL PADRE
if($_POST['prod_parent_id']<>'') 
	$prod = new Prodotto($_POST['prod_parent_id']);
else 
{
	$prod=new Prodotto($_POST); 
	if(sizeof($indici)==0) $prod->prod_tipo='simple';
	else $prod->prod_tipo='variable';
	$prod->inserisci();
	$prod->prod_immagine=$prod->carica_immagine_principale();
	$prod->pa_colore='';
	$prod->pa_taglia='';
	$prod->aggiorna();
}
	
$id_negozio=$_SESSION['id_negozio'];
$id_fornitura=$_POST['fornitura_id'];

if(sizeof($indici)==0){ //se non ha varianti
		$fornitura=new Fornitura($id_fornitura);	
		$fornitura->aggiungi_prodotto($prod->ID, $_POST['qta'], $_POST['prod_prezzo_forn']);
	}
else{
	foreach ($indici as $i)
	{
		$id=$prod->controlla_esistenza_variante($colori[$i], $taglie[$i]);
		
		if(!$id) {
			$id=$prod->aggiungi_variante($colori[$i], $taglie[$i]); //se non esiste la variante la aggiungo
		}

		$fornitura=new Fornitura($id_fornitura); //aggiungo la fornitura
		if($_POST['prod_prezzo_forn']>0)
		$fornitura->aggiungi_prodotto($id, $var_quant[$i],$_POST['prod_prezzo_forn']);
	else
		$fornitura->aggiungi_prodotto($id, $var_quant[$i],$var_prezzo[$i]);
	}
}

header("Location: /prodotti/prodotto_inserisci.php");
