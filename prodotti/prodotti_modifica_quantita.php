<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');

if((empty($_GET['valore']))&&(empty($_GET['id_prodotto']))){
	die("Chiave di ricerca non definita");	
}else{
		$dettaglio=array();
		$query='select ID, quantita_totale, stock_id from prodotti join stocks on ID = prodotto_id where negozio_id = '.$_SESSION['id_negozio'].' ';
		if($_GET['id_prodotto']) 
			$query.=' and ID='.$_GET['id_prodotto'].';';
		else{
			$valore=$_GET['valore'];
			$query.=' and concat(prod_EAN_13," ",SKU," ",prod_codice) like "%'.$valore.'%"';
		}
	$result = mysql_query($query) or die (mysql_error());
	
	while($row=mysql_fetch_array($result,MYSQL_ASSOC))
		array_push($dettaglio,$row);
	
	if(mysql_num_rows($result)<1) {
		die('<a target="_parent" href="/prodotti/prodotti_elenco.php">Tutti i Prodotti</a>');}
		
	}
?>
<head>
<?php
include("../template/metatag.php");
include("../template/css.php");
include("../template/js.php");
?>
</head>
<body>

<?php 
 $negozio = new Negozio($_SESSION['id_negozio']); ?>       
            
 
		<div class="panel panel-primary">
	   		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-search"></i> Gestione scorte prodotti</h3></div>
		    
			<?php 
			foreach($dettaglio as $dettaglio_prodotto){
				$prodotto=new Prodotto($dettaglio_prodotto['ID']);
				?>
		<div class="panel-body">
            <div style="width:50%;float:left; text-align:center;">
                 	<div class="col-lg-12"><img class="hhhimg-responsive" width="200px" src="img/<?php echo $prodotto->prod_immagine?>"/></div>
					<div class="col-lg-12"><img src="<?php echo $prodotto->generaBarcode();?>" alt="testing" /></div>
					<div class="col-lg-12"><h2><?php echo $prodotto->SKU;?></h2></div>
			</div>                    
<div class="row" style="width:50%;float:left">   
<?php
$stock=new Stock($prodotto->ID, $_SESSION['id_negozio']);
?>
<h4><?php echo $prodotto->prod_nome.' '.$prodotto->pa_colore.' '.$prodotto->pa_taglia;?></h4>
<h4>Quantità corrente: <?php echo $stock->quantita_totale; ?></h4>
<div class="row">
		<form method="post" id="form_inserimento" action="prodotto_modifica_quantita-exec.php">   
        <div class="form-group" style="width:50%">
			<label>Qtà venduta:</label>           
			<input type="text" id="qta" value="" name="qta" class="form-control" />
            <input type="hidden" name="id_stock" value="<?php echo $stock->stock_id; ?>">
		</div>
			<button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-save"></i> Salva</button>                    
	    </form>
		</div></div>

</div>
			<?php }?>

 
</div>

</body>


