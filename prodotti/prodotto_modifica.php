<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');

//MostraErrori();

if(empty($_GET['id_prodotto'])){
	die("Prodotto non definito");	
}else{			
	$prod=new Prodotto($_GET['id_prodotto']);	
	
	if($prod->prod_parent_id){
		$prod=new Prodotto($prod->prod_parent_id);	
	}
}

$fornitura = new Fornitura($_GET['id_fornitura']);
$prezzo=$fornitura->get_prezzo_prodotto($prod->ID);
$quantita=$fornitura->get_qty_prodotto($prod->ID);

if($prod->prod_parent_id>0){
	$prod_parent_id=$prod->prod_parent_id;
}else{
	$prod_parent_id=$prod->ID;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include("../template/metatag.php");
include("../template/css.php");
include("../template/js.php");
include("../include/bootstrap-duallistbox.php");
?>
</head>
<body>
<div class="page-container">
<?php include("../template/sidebar.php"); ?>
<div class="page-content">
<?php 
include("../template/top.php"); 
include("../template/breadcrumb.php");
?>
<div class="page-content-wrap">
	<div class="col-lg-12">
    <h2>Modifica prodotto
    
        </h2>
    <div class="alert alert-info">
    	<a class="btn btn-primary" href="/cronjobs/sincronizza_dati.php?passwd=exit2015&redirect=1"><i class="fa fa-refresh"></i> Sincronizza dati</a>
    </div>
<form method="post" action="prodotto_modifica_exec.php?id_prodotto=<?php echo $prod->ID;if(isset($_GET['editforn'])) echo '&editforn';?>">    
<div class="row">
<?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>  
	<div class="col-lg-9">
<?php } else {?>
<div class="col-lg-12">
<?php } ?>
		<div class="panel panel-primary">
	   		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-search"></i> Anagrafica prodotto</h3></div>
		    <div class="panel-body">
			  <div class="form-horizontal" id="form_inserimento" action="">   
				  <div class="row">
					<label class="control-label col-md-1">Modello:</label>
					<div class="form-group col-md-3">
					    <input type="text" id="prod_nome" name="prod_nome" value="<?php echo $prod->prod_nome; ?>" class="form-control" />
					</div>                  
					<label class="control-label col-md-1" for="">Brand:</label>
					<div class="form-group col-md-3">	
	                    <input class="form-control prod_marca" id="prod_marca" name="prod_marca" value="<?php echo $prod->getBrandNamebyId($prod->prod_id_marca); ?>" />		
					</div>	
				      <label class="control-label col-md-1" for="">SKU:</label>
					  <div class="form-group col-md-3">
					    <input type="text" id="SKU" name="SKU" value="<?php echo $prod->SKU; ?>" class="form-control" />
	    		    </div>					
				  </div>
				  <div class="row">

				      <label class="control-label col-md-1" for="">Codice:</label>
			 		  <div class="form-group col-md-3">
					    <input type="text" id="prod_codice" name="prod_codice" value="<?php echo $prod->prod_codice; ?>" class="form-control" />
	     			</div>
				      <label class="control-label col-md-1" for="prod_EAN_13">EAN:</label>
						<div class="form-group col-md-3"><input type="text" id="prod_EAN_13" name="prod_EAN_13" value="<?php echo $prod->prod_EAN_13; ?>" class="form-control" /></div>

		 		 </div>
				 <hr>
                 <div class="row">                                  
	                <label class="control-label col-md-1">Prezzo:</label>
					<div class="form-group col-md-2">
				    <input id="prod_prezzo_forn" name="prod_prezzo_forn" class="form-control" value="<?php echo $fornitura->get_prezzo_prodotto($prod->ID); ?>"  />
					</div>
                    <label class="control-label col-md-1" id="qta">Qtà:</label>
                    <div class="form-input col-md-2"><input type="text" id="qta" name="qta" class="form-control" value="<?php echo $quantita; ?>" /></div>
                	<label class="control-label col-md-2">Prezzo vendita</label>
                    <div class="form-input col-md-2">
                    	<input name="prod_prezzo_vendita" id="prod_prezzo_vendita" class="form-control" value="<?php echo $prod-> prod_prezzo; ?>">
                        </div>
                </div>     
                    
<?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>
<hr>                    
<div class="row"><div class="col-lg-12">
                	<label>Categorie</label>
                	<?php 
					$query_categorie = mysql_query("SELECT * FROM categorie ORDER BY categoria_nome ASC") or die (mysql_error());
					?>
                    <select size="10" id="ids_categorie" multiple="multiple" name="prod_ids_categorie[]">
                    <?php	while($res_categorie=mysql_fetch_array($query_categorie)){	?>
                    	<option value="<?php echo $res_categorie['categoria_wp_id']; ?>"><?php echo $res_categorie['categoria_nome']; ?></option>
                    <?php	}	?>
                    </select>
                </div></div>
<?php } ?>                         
                </div>    

	    </div></div>
	    </div>
       <?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>  
       <div class="col-lg-3">
	<div class="panel panel-danger">
    	<div class="panel-heading"><h3 class="panel-title">Miglior prezzo sul web</h3></div>
    	<div class="panel-body"><div id="RiquadroTrovaprezzi"><?php echo file_get_contents('http://mag.ariota.it/prodotti/cerca_prezzo.php?modello='.str_replace(' ','+',$prod->prod_nome)); ?></div></div>
    </div>    </div>
	   <?php } ?>
</div>
       
       
      <div class="panel panel-success">
      	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i> Descrizione</h3></div>
        <div class="panel-body">
      	                 <div class="row">

	                 <div class="form-input col-md-12">
	                 	<textarea class="ckeditor" name="prod_descrizione" id="prod_descrizione"><?php echo $prod->prod_descrizione; ?></textarea>
                     </div>
                 </div>
        
        </div>
      </div>  
        
        
      <div class="panel panel-warning">
		<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-table"></i> Varianti</h3></div>
	    <div class="panel-body">
           <div class="row elenco_varianti">
<?php  
$array_attributi = $prod->getAttributiVariabili(); 

if(in_array('pa_colore',$array_attributi)){ ?>          
        		<div class="form-group col-md-1"><label class="control-label" for="pa_colore">Colore</label></div>
<?php } ?>
<?php if(in_array('pa_taglia',$array_attributi)){?>          
     			<div class="form-group col-md-1"><label class="control-label" for="pa_taglia">Taglia</label></div>
<?php } ?>
                <div class="form-group col-md-2"><label class="label-description" for="var_SKU">SKU</label></div>
			    <div class="form-group col-md-2"><label class="label-description" for="">Codice</label></div>
			    <div class="form-group col-md-2"><label class="label-description" for="">EAN13</label></div>
			    <div class="form-group col-md-1"><label class="label-description" for="">Pr. Unitario</label></div>
			    <div class="form-group col-md-1"><label class="label-description" for="">Quant.</label></div>                
           </div>

		   	
<?php $figli=$fornitura->get_elenco_prodotti_figli($prod->ID);
while($figlio = mysql_fetch_assoc($figli)){		?>   
<div class="row variante">
<?php if(in_array('pa_colore',$array_attributi)){ ?>              
			  <div class="form-group col-md-1"><input class="form-control pa_colore" id="pa_colore" name="pa_colore[]" value="<?php echo $figlio['pa_colore'];?>"/></div>
<?php } ?>              
<?php if(in_array('pa_taglia',$array_attributi)){?>          
			  <div class="form-group col-md-1"><input class="form-control pa_taglia" id="pa_taglia" name="pa_taglia[]" value="<?php echo $figlio['pa_taglia'];?>"/></div>
<?php } ?>              
			  <div class="form-group col-md-2"><input readonly class="form-control" id="var_SKU" name="var_SKU[]" value="<?php echo $figlio['SKU'];?>"/></div>
			  <div class="form-group col-md-2"><input class="form-control" id="var_codice" name="var_codice[]" value="<?php echo $figlio['prod_codice'];?>"/></div>
			  <div class="form-group col-md-2"><input class="form-control" id="var_EAN13" name="var_EAN13[]" value="<?php echo $figlio['prod_EAN_13'];?>"/></div>  
			  <div class="form-group col-md-1"><input class="form-control" id="var_prezzo" name="var_prezzo[]" value="<?php echo $figlio['prezzo'];?>"></div>
			  <div class="form-group col-md-1"><input class="form-control" id="var_quant" name="var_quant[]" value="<?php echo $figlio['quantita'];?>"></div>
			  <div class="form-input col-md-2">
			      <a class="btn small btn-primary aggiungi_variante" title=""><i class="fa fa-plus"></i></a>
				  <a onclick="$(this).parent().parent().remove();" class="btn btn-danger" title=""><i class="fa fa-trash-o"></i></a>
			  </div>     			  
		  </div>
<?php } ?>
	    </div>
      </div>
      <div class="panel panel-danger">
      	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-camera"></i> Immagini</h3></div>
	    <div class="panel-body">
			  <div class="row">
				  <div class="col-md-6"><input type="file" name="prod_immagine" id="prod_immagine" value="<?php echo 'img/'.$prod->prod_immagine;?>"></div>
				  <div class="col-md-6"><img class="img-thumbnail" src="<?php echo '../img/'.$prod->prod_immagine;?>"></div>
		  	  </div>   
	    </div>
      </div>
      <input type="hidden" name="prod_parent_id" id="prod_parent_id" value="<?php echo $prod_parent_id; ?>" />
	<input type="hidden" name="fornitura_id" value="<?php echo $_GET['id_fornitura']; ?>" />
      <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Salva modifiche</button>


</form>      
</div>
    
    </div>
	<div class="col-lg-12"><?php echo MostraBackLink(); ?></div>
</div>

</div>
</div>
<p>
  <script>function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#prev_prod_immagine').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#prod_immagine").change(function(){
        readURL(this);
    });
	
	</script>
</p>
<p>&nbsp; </p>
<script>

$(document).ready(function() {
 
	$("#ids_categorie").bootstrapDualListbox();	
	
    $('#prod_nome').attr('contenteditable','contenteditable');
	$('#prod_marca').attr('contenteditable','contenteditable');
	$('#SKU').attr('readonly','readonly');
	$('#prod_codice').attr('readonly','readonly');
	$('#prod_EAN_13').attr('readonly','readonly');
	$('.note-editable').attr('contenteditable',false);
	$('#prod_immagine').attr('src','img/'+obj.prod_immagine);
	
$(document).on('click','a.aggiungi_variante',function(){
	elemento=$('.variante:last').clone().find('input').val('').end();
	$('.variante:last').after(elemento);
 });
	
	
	
	var modello=$('#prod_nome').val();
<?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>    	
	$("#ids_categorie option").each(function(){
		<?php 
		$array_categorie = json_decode($prod->prod_ids_categorie);
		foreach($array_categorie as $categoria){
		?>
		 	if($(this).val()==<?php echo $categoria; ?>){
				$(this).prop('selected','selected');					
			}
		<?php
		}
		?>
	});
	$('#ids_categorie').bootstrapDualListbox('refresh');
<?php } ?>
//	$('#RiquadroTrovaprezzi').load('cerca_prezzo.php?modello='+modello);
});
<?php //} ?>


</script>	

</body>
</html>
