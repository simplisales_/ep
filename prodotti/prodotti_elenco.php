<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include("../template/metatag.php");
include("../template/css.php");
include("../template/js.php");
include("../include/jtable.php");
include("../include/fancybox.php");
?>
</head>
<body>
<div class="page-container">
<?php include("../template/sidebar.php"); ?>
<div class="page-content">
<?php 
include("../template/top.php"); 
include("../template/breadcrumb.php");
?>
<div class="page-content-wrap">

<div class="page-title"><h2>Elenco Prodotti</h2></div>
<div class="col-lg-12"><div class="alert alert-info" style="padding:7px">
	<button id="RicaricaDati" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> Ricarica dati</button>
</div></div>
<?php

$query_prodotti="SELECT ID, negozio_id, SKU, prod_codice, prod_EAN_13, prod_nome, pa_colore, pa_taglia, brand_nome, sum(quantita_totale) as gran_totale ";
$query_prodotti.=" from prodotti LEFT JOIN stocks on ID = prodotto_id join brands on prod_id_marca = brand_id";
$query_prodotti.=" where prod_tipo ='simple' ";
if($_SESSION['negozio_livello']!="ADMIN"){
	$query_prodotti.=" AND negozio_id='".$_SESSION['id_negozio']."'";
}
$query_prodotti.=" group by ID ";	
$fp = fopen('/log/queries.log', 'w');
fwrite($fp, $query_prodotti);
fclose($fp);

$result=mysql_query($query_prodotti) or die (mysql_error());

$numero_prodotti=mysql_num_rows($result);
$per_page=100;
$tot_pages=ceil($numero_prodotti/$per_page);
$current_page=$_GET['page'] ?: 1;
$primo=($current_page-1)*$per_page;
$p=0;

?>  
<div class="col-lg-12"><div id="ProdottiTableContainer"></div></div>
<div class="col-lg-12"><?php MostraBackLink(); ?></div>


</div></div></div>

<script type="text/javascript">
$(document).ready(function () {		

		$('#ProdottiTableContainer' ).tooltip({ position: {my: 'center bottom', at: 'center top-10'}});							

        $('#ProdottiTableContainer').jtable({       	
			paging: true,
			pageSize: '<?php echo $per_page; ?>',
			sorting: true,
			defaultSorting: 'ID ASC',
			isCellEditable: true,
			selecting:true,
		    multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column	
            actions: {
                listAction: 'dati/get_prodotti.php?action=list&<?php echo $_SERVER['QUERY_STRING']; ?>',
             //   deleteAction: 'dati/get_listino.php?action=delete',
            },
		    fields: {
                ID: {
                    key: true,
					list:false
                },		
                prod_immagine: {
					title: 'Img.',
					display: function (data) {
						return '<a class="fancybox" href="/prodotti/img/'+data.record.prod_immagine+'"><img class="img-thumbnail" src="/timthumb.php?src=/prodotti/img/'+data.record.prod_immagine+'&w=20&h=20"></a>';
					}
                },							
				prod_codice: {
					title: 'Codice',
					display: function (data) {
						return '<span title="Città">'+data.record.prod_codice+'</span>'
					}
				},		
				SKU:{
					title: 'Sku',
					display: function (data) {
						return '<span title="Sku">'+data.record.SKU+'</span>'
					}		
				},
				prod_EAN_13: {
					title: 'EAN',
					display: function (data) {
						return '<span title="Città">'+data.record.prod_EAN_13+'</span>'
					}
				},								
				prod_nome:{
					title: 'Modello',
					display: function (data) {
						return '<span title="Nome prodotto">'+data.record.prod_nome+'</span>'
					}
				},	
				pa_colore:{
					title: 'Colore',
					display: function (data) {
						return '<span title="Data inserimento">'+data.record.pa_colore+'</span>'
					}
				},	
				pa_taglia:{
					title: 'Taglia',
					display:function(data){
						return '<span title="Indirizzo">'+data.record.pa_taglia+'</span>'	
					}	
				},
				brand_nome:{
					title: 'Marca.',
					display:function(data){
						return '<span title="Prov.">'+data.record.brand_nome+'</span>'	
					}	
				},
				gran_totale:{
					title: 'Totale.',
					display:function(data){
						return '<span title="Prov.">'+data.record.gran_totale+'</span>'	
					}	
				},	
				venditaBanco: {
					title: '',
                	width: '40%',
					sorting: false,
                	display: function(data) {		
				
	                 	  	return '<a title="Modifica" class="btn btn-success btn-sm fancybox" href="/prodotti/prodotti_modifica_quantita.php?id_prodotto=' + data.record.ID + '" data-fancybox-type="iframe"><i class="fa fa-shopping-cart"></i> Vendita al banco</a>';

 

                	}
	            },				
				<?php /*?><?php if($_SESSION['negozio_livello']=="ADMIN"){ ?>
				editButton: {
					title: '',
                	width: '40%',
					sorting: false,
                	display: function(data) {		
				
	                 	  	return '<a title="Modifica" class="btn btn-success btn-sm" href="prodotto_modifica.php?id=' + data.record.ID + '"><i class="fa fa-edit"></i></a>';

                	}
	            },

				deleteButton: {
					title: '',
                	width: '40%',
					sorting: false,
                	display: function(data) {						
                 	  	return '<a title="Elimina" class="Elimina btn btn-danger btn-sm" href="prodotto_elimina.php?id=' + data.record.ID + '"><i class="fa fa-trash-o"></i></a>';
                	}
	            },	
				<?php } else { ?>				
				modificaQuantita: {
					title: '',
                	width: '40%',
					sorting: false,
                	display: function(data) {						
                 	  	return '<button class="btn btn-info btn-sm" onclick="ModificaQta('+data.record.ID+');"><i class="fa fa-edit"></i></button>';
                	}
	            },	
				<?php } ?><?php */?>
            },

        });
		
  		$('#ProdottiTableContainer').jtable('load'); 
		


		
})
	$('#RicaricaDati').click(function(){ 
		<?php if($_GET['filtri_attivi']==1){ ?>
		    window.location.href = 'prodotti_elenco.php';
		<?php }else{ ?>
			$('#ProdottiTableContainer').jtable('reload');		
		<?php }	?>
	})
	
	

function ModificaQta(prodotto){
	$.fancybox.open([{
        href : 'prodotto_modifica_quantita.php?id=' + prodotto,
	    title : 'Modifica quantità',
		type: 'iframe',
		afterClose: function() {
			$('#ProdottiTableContainer').jtable('reload');
		}
    }]);
}	

	
	</script>
<style>
.jtable{
	
}
</style>  
<style>
#ProductsTableContainer{
    overflow-x: auto;
    width:100%;
}
div.jtable-main-container {
  height:100%;
}
.jtable-column-header{
 height:35px; 
 font-size:12px !important;
}
.jtable{
 font-size:12px !important;
 font-weight:500;
}

div.jtable-main-container > table.jtable > tbody > tr.jtable-row-event {
  background-color: #D9EDF7 !important;
}
.jtable tr.jtable-row-odd,
tr.second {
  background-color: #ccc;
}
.jtable tr.third {
  background-color: #ddd;
}

span.Record{
 color:#404040 !important; 
 font-family:Arial, Helvetica, sans-serif;
 font-size:12px; 
}

span.RecordSmall{
 color:#404040 !important; 
 font-family:Arial, Helvetica, sans-serif;
 font-size:10px;
}
</style>  
</body>
</html>
