<?php
require_once('../config.inc.php');
require_once('../include/verifica_login.php');

include("../template/js.php");

$stock = new Stock($_POST['id_stock']);

$stock->quantita_totale-=$_POST['qta'];

if($stock->quantita_totale>=0){
	$stock->aggiorna();	
}else{
	die("Non puoi sottrarre dallo stock una quantità maggiore di quella presente");	
}
?>
<script>
$(document).ready(function() {
    parent.$.fancybox.close();
	parent.$('#ProdottiTableContainer').jtable('reload');		
});
</script>