<?php
require_once('../../config.inc.php');
require_once('../../include/verifica_login.php');

try{
	//Getting records (listAction)
	if($_GET["action"] == "list"){
		
		if(isset($_GET["jtStartIndex"])){
			$start_index = $_GET["jtStartIndex"];	
		}else{
			$start_index = 0;	
		}
		
		if(isset($_GET["jtPageSize"])){
			$page_size = $_GET["jtPageSize"];	
		}else{
			$page_size = 10;	
		}
		
$query_prodotti="SELECT ID, negozio_id, SKU, prod_codice, prod_EAN_13, prod_nome, pa_colore, pa_taglia, brand_nome, coalesce(sum(quantita_totale),0) as gran_totale,prod_immagine ";
$query_prodotti.=" from prodotti LEFT JOIN stocks on ID = prodotto_id join brands on prod_id_marca = brand_id";
$query_prodotti.=" where prod_tipo ='simple' ";

$id_negozio=$_GET['id_negozio'];

if(($_SESSION['negozio_livello']<>"ADMIN")){
	$query_prodotti.=" AND negozio_id='".$id_negozio."'";
}
else{
	if($id_negozio)	{
	$query_prodotti.=" AND negozio_id='".$id_negozio."'";
	}
}
$query_prodotti.=" group by ID ";

$fp = fopen('/log/queries.log', 'w');
fwrite($fp, $query_prodotti);
fclose($fp);
	
		
		//Get record count
		$result = mysql_query($query_prodotti) or die (mysql_error());
		


		$recordCount = mysql_num_rows($result);


		$query_filtro='';
		
		if($_POST['cont_nominativo']){
		    $filtro_particolare=explode(':',$_POST['cont_nominativo']);
		    $num_tags = count($filtro_particolare);

		    if ($num_tags==2){

			$campo=trova_campo(trim($filtro_particolare[0]));
			
			if ($campo<>'')	$query_filtro.=" AND ".$campo." LIKE '%".trim($filtro_particolare[1])."%' ";

		    }
		    elseif($num_tags<2){
			$filtro=explode(' ',$_POST['cont_nominativo']);
			foreach($filtro as $partefiltro)
			{
			    $query_filtro.=" AND concat(cont_nominativo,cont_struttura,cont_categoria,cont_email,cont_note,cont_cap,cont_paese,cont_telefono,cont_cellulare,cont_sito_internet,cont_loc_italia,cont_source,cont_citta,cont_indirizzo) LIKE '%".$partefiltro."%' ";
			}
			
		    }
		    if($query_filtro<>''){
		    $query_filtro=substr($query_filtro,4);
			$query_filtro='WHERE '.$query_filtro;}
		}

		if(isset($_GET["jtSorting"])){
			$sorting = $_GET["jtSorting"];
		}else{
			$sorting = "ID ASC";			
		}

		if(isset($_GET["jtStartIndex"])){
			$start_index = $_GET["jtStartIndex"];	
		}else{
			$start_index = 0;	
		}
		
		if(isset($_GET["jtPageSize"])){
			$page_size = $_GET["jtPageSize"];	
		}else{
			$page_size = 10;	
		}
		
		//Get records from database
		$result = mysql_query($query_prodotti." ".$query_filtro." ORDER BY " . $sorting . " LIMIT " . $start_index . "," . $page_size . ";");
		
		$fp = fopen('../../log/queries.log', 'w+');
fwrite($fp, $query_prodotti." ".$query_filtro." ORDER BY " . $sorting . " LIMIT " . $start_index . "," . $page_size . ";");
fclose($fp);
		//Add all records to an array
		$rows = array();
				
		while($row = mysql_fetch_array($result)){
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		//Insert record into database
		$result = mysql_query("INSERT INTO people(Name, Age, RecordDate) VALUES('" . $_POST["Name"] . "', " . $_POST["Age"] . ",now());");
		
		//Get last inserted record (to return to jTable)
		$result = mysql_query("SELECT * FROM people WHERE PersonId = LAST_INSERT_ID();");
		$row = mysql_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		//Update record in database
		$result = mysql_query("UPDATE people SET Name = '" . $_POST["Name"] . "', Age = " . $_POST["Age"] . " WHERE PersonId = " . $_POST["PersonId"] . ";");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		//Delete from database
		$result = mysql_query("DELETE FROM people WHERE PersonId = " . $_POST["PersonId"] . ";");

		//Return result to jTable 
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysql_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>