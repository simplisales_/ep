
<?php

header('Content-Type: text/html; charset=utf-8');
function get_string_between($string, $start, $end){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
}

function getHTML($url,$timeout)
{
    $ch = curl_init($url); // initialize curl with given url
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, true);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
    $data = @curl_exec($ch);
    curl_close($ch);
    return $data;
}

$modello=str_replace(' ','+',trim($_GET['modello']));

//$html=getHTML("http://www.amazon.it/s/ref=nb_sb_noss?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Daps&field-keywords=".urlencode($modello)."&qid=1417342942&rnid=1640607031&sort=price-desc-rank",10);
$html=getHTML("http://www.trovaprezzi.it/categoria.aspx?libera=".$modello);
//$html=getHTML("http://www.amazon.it/s/ref=sr_st_price-asc-rank?__mk_it_IT=%C3%85M%C3%85Z%C3%95%C3%91&keywords='".urlencode($modello)."'&qid=1417343766&rh=n%3A524006031%2Ck%3A".urlencode($modello)."&sort=price-asc-rank");
//$form_ordinamento=get_string_between($html, 'result_0', "result_1");
if(strstr($html, 'catsMI')){
$html=get_string_between($html, '<div class="catsMI">', "</div>");
$html=get_string_between($html, '<a href="','"');

$html=getHTML('http://www.trovaprezzi.it'.$html);
}
$img = get_string_between($html, 'http://images.trovaprezzi.it/it-100x100/', ".jpg");
$img ='http://images.trovaprezzi.it/it-300x300/'.$img.'.jpg';
$img=str_replace('._AA160_','',$img);
$title = get_string_between($html, '<h3 class="psrt">', "</h3>");
if (strpos($title,'-') !== false) {
    $titles = explode("-", $title);
    $title=$titles[0];
}
if(trim($title=="")){
    $title = get_string_between($html, 'Totale:', "</span>");
    if (strpos($title,'-') !== false) {
    $titles = explode("-", $title);
    $title=$titles[0];
}
}


if(trim($title=="")) {
    $title="Nessun risultato";
    echo'
<h5 class="heading-1 bg-red radius-all-4 btn text-center display-block pad10A clearfix">
    <div class="heading-content">
        '.$title.'
    </div>
</h5>';
}
else{
echo'
<div class="col-md-12">                        
	<a href="#" class="tile tile-default">
		€ '.$title.'
		<img src="'.$img.'" style="width:100px;"/>
	</a>                        
</div>';
}  
?>