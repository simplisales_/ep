<?php 
date_default_timezone_set('Europe/Rome');
session_start();

/*Configurazione applicazione */
$nome_applicazione="Warehouse Manager";
$url_applicazione = "http://whmanager.ariota.it";

/*Configurazione negozio */
$UrlNegozio = "http://www.exitprice.it/";

/* Parametri connessione database */
$___DB_host = "localhost";
$___DB_user = "ariota_exit";
$___DB_pass = "4dW_2zdU*s!a";
$___DB_name = "ariota_exitprice";

//-----------------------------//

$Css = $url_applicazione."/template/css";
$Js = $url_applicazione."/template/js";

$path_applicazione= $_SERVER['DOCUMENT_ROOT'];
$url_immagini_applicazione = $url_applicazione."/immagini";

/* Parametri email */
$email_negozio['email']= "info@exitprice.it";
$email_negozio['smtp'] = "smtp.exitprice.it";
$email_negozio['port'] = "25";
$email_negozio['username'] = "info@exitprice.it";
$email_negozio['password'] = "ht724B21";

/*Parametri Google Webmaster e Analytics */
$ga['username'] = "";
$ga['password'] = "";
$ga['id_profilo'] = "";
$ga['data_inizio'] = "";
$ga['ua'] = "";

/*	Variabili temporali	*/
$data_odierna = date('Y-m-d H:i:s');
$data_odierna_senza_ora = explode(" ",$data_odierna);
$data_ieri = date('Y-m-d', strtotime('-1 days'));
$data_odierna_3_gg = date('Y-m-d',strtotime($data_odierna_senza_ora[0]." -3 days"));
$data_odierna_30_gg = date('Y-m-d', strtotime('-1 month'));
$data_odierna_15_gg = date('Y-m-d', strtotime('-15 days'));
$data_odierna_2_mesi = date('Y-m-d', strtotime('-2 month'));
$data_odierna_3_mesi = date('Y-m-d', strtotime('-3 month'));
$data_odierna_6_mesi = date('Y-m-d', strtotime('-6 month'));
$data_odierna_9_mesi = date('Y-m-d', strtotime('-9 month'));
$data_odierna_12_mesi = date('Y-m-d', strtotime('-12 month'));


include_once('include/funzioni.php' );
include("classi/SmartImage.class.php");		//	Classe per manipolazione immagini

/* Classi */
include('classi/mysql_class.php' );
include_once('classi/prodotto_class.php');
include_once('classi/fornitura_class.php');
include_once('classi/negozio_class.php');
include_once('classi/ordine_class.php');
include_once('classi/stock_class.php');
include_once('classi/class.phpmailer.php');
include_once('classi/class.smtp.php');
include_once('classi/cliente_class.php');

include_once('include/genera_campi_form.php');